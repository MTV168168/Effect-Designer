#include "StdAfx.h"
#include "DUIPreviewPanel.h"

#define BOTTOMHEIGHT 45
DUIPreviewPanel::DUIPreviewPanel()
{
}

DUIPreviewPanel::~DUIPreviewPanel()
{
}

DMCode DUIPreviewPanel::DV_UpdateChildLayout()
{
	do 
	{
		if (!g_pMainWnd->m_pSDKPreviewWnd)
			break;

		DUIWindow* pParent = DM_GetWindow(GDW_PARENT);
		if (!pParent)
			break;

		CRect rtParent;
		pParent->DV_GetClientRect(rtParent);
		if (rtParent.Width() < 1)
			break;
		int iPanelHeight = rtParent.Width() * 16 / 9 + BOTTOMHEIGHT; //宽高比 算出高度
		int nShrink = (rtParent.Height() - iPanelHeight) / 2;
		nShrink = nShrink < 0 ? 0 : nShrink;
		CRect rtWnd = CRect(rtParent.left, rtParent.top + nShrink, rtParent.right, rtParent.bottom - nShrink);
		if (rtWnd == m_rcWindow)//DM_UpdateLayout 会调用DV_UpdateChildLayout  DV_UpdateChildLayout会调用DM_UpdateLayout 不跳出 导致死循环 
		{
			g_pMainWnd->m_pSDKPreviewWnd->SetWindowPos(0, m_rcWindow.left, m_rcWindow.top, m_rcWindow.Width(), m_rcWindow.Height() - BOTTOMHEIGHT, SWP_NOACTIVATE);
			break;
		}
		DM_UpdateLayout(rtWnd);
	} while (false);
	return DUIWindow::DV_UpdateChildLayout();
}