#include "StdAfx.h"
#include "EDListBoxEx.h"
#include "DMDragWnd.h"

namespace ED
{
	EDListBoxEx::EDListBoxEx()
		: m_bSwapLine(true)
		, m_bDragging(false)
		, m_dwHitTest(-1)
		, m_dwDragTo(-1)
	{
		m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, Subscriber(&EDListBoxEx::OnSelectChangedEvent, this));
	}

	EDListBoxEx::~EDListBoxEx()
	{

	}

	LRESULT EDListBoxEx::OnMouseEvent(UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		LRESULT lRet = 0;
		do
		{
			if (m_iSelItem > -1)
			{
				DMSmartPtrT<DUIItemPanel> pPanel = m_DMArray[m_iSelItem]->pPanel;
				DUIRichEdit* pEdit = pPanel->FindChildByNameT<DUIRichEdit>(RESLIB_EDIT); DMASSERT(pEdit);
				if (pEdit && pEdit->DM_IsVisible())
				{
					SetMsgHandled(FALSE);
					break;
				}
			}

			CPoint pt(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
			// 加入拖动判断
			if (m_bSwapLine)
			{
				if (WM_LBUTTONDOWN == uMsg)
				{
					KillAllVisibleEditFocus();
					m_ptClick = pt;
					CPoint pt2 = pt;
					m_dwHitTest = HitTest(pt2);// pt会在HitTest中自动改变
				}
				else if (WM_MOUSEMOVE == uMsg)
				{
					if (m_bDragging || wParam & MK_LBUTTON)
					{
						if (!m_bDragging)
						{
							m_bDragging = true;      /// 开始移动
							if (-1 != m_dwHitTest)
							{
								m_dwDragTo = m_dwHitTest;
								CRect rcItem = GetItemRect(m_dwHitTest);
								CreateDragCanvas(m_dwHitTest);//  创建拖动时的画布
								CPoint pt = m_ptClick - rcItem.TopLeft();
								DMDragWnd::DragBegin(m_pDragCanvas, pt, 0, 150, LWA_ALPHA | LWA_COLORKEY);
							}
						}

						if (-1 != m_dwHitTest)
						{
							CPoint pt0 = pt;
							DWORD dwDragTo = HitTest(pt0);

							// 限制拖动范围
							CRect rcItem = GetItemRect(m_dwHitTest);
							CPoint pt2(pt.x, pt.y);
							pt2.x = m_ptClick.x;// 强制坚直边对齐
							if (pt2.y < m_rcWindow.top + DMDragWnd::ms_pCurDragWnd->m_ptHot.y)// 强制不能拖出listboxex的顶部
							{
								pt2.y = m_rcWindow.top + DMDragWnd::ms_pCurDragWnd->m_ptHot.y;
							}

							if (pt.y + rcItem.Height() > m_rcWindow.bottom + DMDragWnd::ms_pCurDragWnd->m_ptHot.y)// 强制不能拖出listboxex的底部
							{
								pt2.y = m_rcWindow.bottom + DMDragWnd::ms_pCurDragWnd->m_ptHot.y - rcItem.Height();
							}

							ClientToScreen(GetContainer()->OnGetHWnd(), &pt2);
							if (m_dwDragTo != dwDragTo)
							{
								m_dwDragTo = dwDragTo;
							//	DrawDraggingState(dwDragTo);// 自动调整item位置
							}

					/*		do
							{
								CRect rcClient;
								DV_GetClientRect(rcClient);
								CRect rcItem(0, 0, rcClient.Width(), m_DMArray[m_dwHitTest]->nHeight);

								// 创建拖动画布
								m_pDragCanvas.Release();
								DMSmartPtrT<IDMRender> pRender;
								g_pDMApp->GetDefRegObj((void**)&pRender, DMREG_Render);
								pRender->CreateCanvas(rcItem.Width(), rcItem.Height(), &m_pDragCanvas);

								if (NULL == m_pDragCanvas)
								{// 没有拖动时的临时画布
									break;
								}

								DV_SetDrawEnvironEx(m_pDragCanvas);
								if (m_DMArray[m_dwHitTest]->pPanel)
								{
									m_DMArray[m_dwHitTest]->pPanel->DrawItem(m_pDragCanvas, rcItem);
								}

								CRect rt = CRect(10, 20, pt.x, 25);
								//m_pDragCanvas->SelectObject();
								//	m_pDragCanvas->DrawRectangle()
								if (pt.x < 180)
									m_pDragCanvas->FillRectangle(rt);

								CSize sz;
								if (0 != (m_pDragCanvas->GetSize(sz)))
								{
									break;
								}
								if (0 == sz.cx || 0 == sz.cy)
								{
									break;
								}

								HDC hdc = DMDragWnd::ms_pCurDragWnd->GetDC();
								HDC dcMem = m_pDragCanvas->GetDC();
								BLENDFUNCTION bf = { AC_SRC_OVER, 0, 150, AC_SRC_ALPHA };
								DMDragWnd::ms_pCurDragWnd->UpdateLayeredWindow(hdc, &CPoint(0, 0), &sz, dcMem, &CPoint(0, 0), LWA_ALPHA | LWA_COLORKEY, &bf, LWA_ALPHA);

								m_pDragCanvas->ReleaseDC(dcMem);
								DMDragWnd::ms_pCurDragWnd->ReleaseDC(hdc);
							} while (false);  */

							DMDragWnd::DragMove(pt2);
						}
					}
				}
				else if (WM_LBUTTONUP == uMsg)
				{
					DMDragWnd::DragEnd();
					m_pDragCanvas.Release();

					if (m_dwDragTo != m_dwHitTest &&-1 != m_dwDragTo)
					{
						EDJSonParserPtr pDragData = (EDJSonParserPtr)GetItemData(m_dwHitTest);
						EDJSonParserPtr pDragToData = JSPARSER_FIRST;
						if (m_dwDragTo > m_dwHitTest) //向下拖拽
						{
							pDragToData = (EDJSonParserPtr)GetItemData(m_dwDragTo);
						}
						else//向上拖拽
						{							
							if ((int)m_dwDragTo - 1 >= 0)
							{
								pDragToData = (EDJSonParserPtr)GetItemData(m_dwDragTo-1);
							}							
						}
						if (pDragData)
						{
							pDragData->MoveParserItemToNewPos(pDragToData);
						}

						LPLBITEMEX  pItem = m_DMArray[m_dwHitTest];
						DMSmartPtrT<DUIItemPanel>& pPanel = m_DMArray[m_dwHitTest]->pPanel;
						DUIStatic* pStatic = pPanel->FindChildByNameT<DUIStatic>(RESLIB_STATIC); DMASSERT(pStatic);
						std::wstring txt = pStatic->m_pDUIXmlInfo->m_strText;
						m_DMArray.RemoveAt(m_dwHitTest);
						m_DMArray.InsertAt(m_dwDragTo, pItem);
						for (size_t i = 0; i < m_DMArray.GetCount(); i++)//lzlong 强制刷新选中状态
						{
							if (i != m_dwDragTo)
								m_DMArray[i]->pPanel->ModifyState(0, DUIWNDSTATE_Check);
						}					
						UpdateItemPanelId();// 更新下Panel的唯一ID
					}

					CPoint pt2 = pt;
					m_dwHitTest = HitTest(pt2);
					if (m_bDragging && -1 != m_dwHitTest)
						m_iHoverItem = m_dwHitTest;

					m_dwDragTo = -1;
					m_bDragging = false;
					DM_Invalidate();
				}
			}
			if (m_bDragging)
			{
				m_iHoverItem = m_dwHitTest;
				break;
			}

			if (WM_LBUTTONUP == uMsg && m_iHoverItem != m_iSelItem)
			{
				SetCurSel(m_iHoverItem);
			}

			if (m_pCapturePanel)
			{
				CRect rcItem;
				m_pCapturePanel->OnGetContainerRect(rcItem);
				pt.Offset(-rcItem.TopLeft());///< 转换成面板坐标
				lRet = m_pCapturePanel->OnFrameEvent(uMsg, wParam, MAKELPARAM(pt.x, pt.y));
				break;
			}

			if (m_pDUIXmlInfo->m_bFocusable
				&& (uMsg == WM_LBUTTONDOWN || uMsg == WM_RBUTTONDOWN || uMsg == WM_LBUTTONDBLCLK))
			{
				DV_SetFocusWnd();
			}

			int iHoverItem = HitTest(pt);
			if (iHoverItem != m_iHoverItem)
			{
				int iOldHoverItem = m_iHoverItem;
				SetCurHover(iHoverItem);
				if (-1 != iOldHoverItem)
				{
					m_DMArray[iOldHoverItem]->pPanel->OnFrameEvent(WM_MOUSELEAVE, 0, 0);
				}

				if (-1 != m_iHoverItem)
				{
					m_DMArray[m_iHoverItem]->pPanel->OnFrameEvent(WM_MOUSEHOVER, wParam, MAKELPARAM(pt.x, pt.y));
				}
			}

			if (WM_LBUTTONDOWN == uMsg&& -1 != m_iSelItem && m_iSelItem != m_iHoverItem)
			{///原有行失去焦点
				m_DMArray[m_iSelItem]->pPanel->m_FocusMgr.SetFocusedWnd(NULL);
			}

			if (-1 != m_iHoverItem)
			{
				m_DMArray[m_iHoverItem]->pPanel->OnFrameEvent(uMsg, wParam, MAKELPARAM(pt.x, pt.y));//panel的子项得到hover时，会让原来得到hover的panel失去hover
			}

			if (-1 != m_iHoverItem)
			{
				m_DMArray[m_iHoverItem]->pPanel->ModifyState(DUIWNDSTATE_Hover, 0);//listctrlex的item下还有一层window,所以listctrlex失去hover的是window,不用担心
			}
		} while (false);
		SetMsgHandled(FALSE);

		return lRet;
	}

	void EDListBoxEx::OnRButtonDown(UINT nFlags, CPoint pt)
	{
		do
		{
			m_iHoverItem = HitTest(pt);
			if (m_iHoverItem != m_iSelItem && m_iHoverItem > -1)
			{
				SetCurSel(m_iHoverItem);
			}
			KillAllVisibleEditFocus();

			DMXmlDocument Doc;
			g_pDMApp->InitDMXmlDocument(Doc, XML_LAYOUT, L"ds_menu_respanel");
			DMXmlNode XmlNode = Doc.Root();
			XmlNode.SetAttribute(L"MaxWidth", L"130");
			XmlNode.SetAttribute(L"itemhei", L"34");

			DMXmlNode XmlItem = XmlNode.InsertChildNode(XML_ITEM);
			XmlItem.SetAttribute(XML_ID, IntToString(g_ResMenuItem[RESLIBLISTBOX_RENAME - RESLIBLISTBOX_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_ResMenuItem[RESLIBLISTBOX_RENAME - RESLIBLISTBOX_BASE].text);
			XmlItem = XmlNode.InsertChildNode(XML_ITEM);
			XmlItem.SetAttribute(XML_ID, IntToString(g_ResMenuItem[RESLIBLISTBOX_DELETE - RESLIBLISTBOX_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_ResMenuItem[RESLIBLISTBOX_DELETE - RESLIBLISTBOX_BASE].text);

			DUIMenu Menu;
			Menu.LoadMenu(XmlNode);
			POINT pt;
			GetCursorPos(&pt);
			Menu.TrackPopupMenu(0, pt.x, pt.y, GetContainer()->OnGetHWnd());
		} while (false);
	}

	void EDListBoxEx::OnKeyDown(TCHAR nChar, UINT nRepCnt, UINT nFlags)
	{
		if (VK_DELETE == nChar)
			g_pMainWnd->OnCommand(0, RESLIBLISTBOX_DELETE, NULL);
		SetMsgHandled(FALSE);
	}

	void EDListBoxEx::CreateDragCanvas(UINT iItem)
	{
		do
		{
			if (iItem >= (UINT)GetCount())
			{
				break;
			}
			CRect rcClient;
			DV_GetClientRect(rcClient);
			CRect rcItem(0, 0, rcClient.Width(), m_DMArray[iItem]->nHeight);

			// 创建拖动画布
			m_pDragCanvas.Release();
			DMSmartPtrT<IDMRender> pRender;
			g_pDMApp->GetDefRegObj((void**)&pRender, DMREG_Render);
			pRender->CreateCanvas(rcItem.Width(), rcItem.Height(), &m_pDragCanvas);
			DV_SetDrawEnvironEx(m_pDragCanvas);
			if (m_DMArray[iItem]->pPanel)
			{
				m_DMArray[iItem]->pPanel->DrawItem(m_pDragCanvas, rcItem);
			}
		} while (false);
	}

	void EDListBoxEx::DrawDraggingState(DWORD dwDragTo)
	{
		do
		{
			CRect rcClient;
			DV_GetClientRect(&rcClient);
			IDMCanvas *pCanvas = DM_GetCanvas(rcClient, DMOLEDC_PAINTBKGND);
			if (NULL == pCanvas)
			{
				break;
			}
			DUIDrawEnviron DrawEnviron;
			DV_PushDrawEnviron(pCanvas, DrawEnviron);
			CRect rcItem(rcClient.left, rcClient.top, rcClient.left, rcClient.bottom);
			int iDragTo = dwDragTo;
			int iDragFrom = m_dwHitTest;

			CArray<UINT_PTR>items;
			int iCount = (int)GetCount();
			for (int i = 0; i < iCount; i++)
			{
				if (i != iDragFrom) //0项把所有的项都加入，除了拖动的项
				{
					items.Add(i);
				}
			}
			items.InsertAt(iDragTo, iDragFrom);// 把拖动的项加入到拖到的项之后

			int nTotalHeight = 0;
			for (UINT i = 0; i < items.GetCount(); i++)
			{
				if ((nTotalHeight >= m_ptCurPos.y && nTotalHeight < m_ptCurPos.y + rcClient.Height())
					|| nTotalHeight + m_DMArray[items[i]]->nHeight >= m_ptCurPos.y && nTotalHeight + m_DMArray[items[i]]->nHeight < m_ptCurPos.y + m_rcsbClient.Height())
				{
					CRect rcItem(0, 0, rcClient.Width(), m_DMArray[items[i]]->nHeight);
					rcItem.OffsetRect(0, nTotalHeight - m_ptCurPos.y);
					rcItem.OffsetRect(rcClient.TopLeft());
					DrawItem(pCanvas, rcItem, items[i]);
				}
				nTotalHeight += m_DMArray[items[i]]->nHeight;
			}

			DV_PopDrawEnviron(pCanvas, DrawEnviron);
			DM_ReleaseCanvas(pCanvas);
		} while (false);
	}

	DMCode EDListBoxEx::InsertNewItem(const CStringW& strResName, int index /*= -1*/, bool b2DPic, bool bInUse, LPARAM lpData /*= NULL*/)
	{
		DMCode iErr = DM_ECODE_FAIL;
		do
		{
			DMXmlDocument Doc;
			DMXmlNode XmlItem = Doc.Base();
			XmlItem = XmlItem.InsertChildNode(L"item");
			DMXmlNode SkinXmlItem = XmlItem.InsertChildNode(L"static"); SkinXmlItem.SetAttribute(L"name", RESLIB_SKIN);
			SkinXmlItem.SetAttribute(L"pos", L"15,6,@14,@14"); SkinXmlItem.SetAttribute(L"skin", b2DPic ? L"ds_res2Dpic" : L"ds_resMakeuppic");
			DMXmlNode RedDotXmlItem = XmlItem.InsertChildNode(L"static"); RedDotXmlItem.SetAttribute(L"name", RESLIB_REDDOT);
			RedDotXmlItem.SetAttribute(L"pos", L"28,4,@5,@5"); RedDotXmlItem.SetAttribute(L"skin", L"ds_RedDotIco");
			RedDotXmlItem.SetAttribute(L"bvisible", bInUse ? L"0": L"1");
			DMXmlNode StaXmlItem = XmlItem.InsertChildNode(L"static");
			StaXmlItem.SetAttribute(L"pos", L"36,0,-5,-0"); StaXmlItem.SetAttribute(L"name", RESLIB_STATIC); 
			StaXmlItem.SetAttribute(L"text", strResName); StaXmlItem.SetAttribute(L"bdot", L"1");
			StaXmlItem.SetAttribute(L"clrtext", L"pbgra(ef,dd,cd,ff)"); StaXmlItem.SetAttribute(L"align", L"left");
			StaXmlItem.SetAttribute(L"alignv", L"center"); StaXmlItem.SetAttribute(L"font", L"face:微软雅黑,size:12,weight:200");
			DMXmlNode EditXmlItem = XmlItem.InsertChildNode(L"richedit");
			EditXmlItem.SetAttribute(L"name", RESLIB_EDIT); EditXmlItem.SetAttribute(L"pos", L"34,4,-4,-4");
			EditXmlItem.SetAttribute(L"skin", L"ds_editframe"); EditXmlItem.SetAttribute(L"bwantreturn", L"1");
			EditXmlItem.SetAttribute(L"maxbuf", L"100"); EditXmlItem.SetAttribute(L"rcinsertmargin", L"2,1,2,0");
			EditXmlItem.SetAttribute(L"clrcaret", L"pbgra(ff,ff,ff,ff)"); EditXmlItem.SetAttribute(L"clrtext", L"rgba(ff,ff,ff,ff)");
			EditXmlItem.SetAttribute(L"bvisible", L"0"); EditXmlItem.SetAttribute(L"bautosel", L"1"); EditXmlItem.SetAttribute(L"font", L"face:微软雅黑.,size:12,weight:200");
			Init_Debug_XmlBuf(XmlItem);

			int iItem = InsertItem(index, XmlItem);
			SetItemData(iItem, lpData);
			iErr = DM_ECODE_OK;
		} while (false);
		return iErr;
	}

	DMCode EDListBoxEx::UpdateItemInfo(const CStringW& strResName, int index, bool b2DPic, bool bInUse, LPARAM lpData)
	{
		if (index >= GetCount())
			return DM_ECODE_FAIL;

		DMSmartPtrT<DUIItemPanel>& pPanel = m_DMArray[index]->pPanel;
		DUIStatic* pStatic = pPanel->FindChildByNameT<DUIStatic>(RESLIB_STATIC); DMASSERT(pStatic);
		pStatic->SetAttribute(L"text", strResName);
		DUIWindow* pSkinWnd = pPanel->FindChildByNameT<DUIStatic>(RESLIB_SKIN); DMASSERT(pSkinWnd);
		pSkinWnd->SetAttribute(L"skin", b2DPic ? L"ds_res2Dpic" : L"ds_resMakeuppic");
		DUIWindow* pRedDotWnd = pPanel->FindChildByNameT<DUIStatic>(RESLIB_REDDOT); DMASSERT(pRedDotWnd);
		pRedDotWnd->SetAttribute(L"bvisible", bInUse ? L"0" : L"1");
		return DM_ECODE_OK;
	}

	void EDListBoxEx::DM_OnKillFocus()
	{
		DUIListBoxEx::DM_OnKillFocus();
		if (g_pMainWnd->m_pEffectTreeCtrl->GetSelectedItem()) //右边的属性栏combobox下拉导致整个窗口失去焦点而触发
		{
			int iSele = GetCurSel();
			if (iSele > -1)
			{
				m_iSelItem = -1;
				m_DMArray[iSele]->pPanel->ModifyState(0, DUIWNDSTATE_Check); //listctrl取消选中
				RedrawItem(iSele);
			}
		}
		KillAllVisibleEditFocus();
	}

	DMCode EDListBoxEx::KillAllVisibleEditFocus()
	{
		for (size_t index = 0; index < m_DMArray.GetCount(); index++)
		{
			DMSmartPtrT<DUIItemPanel>& pPanel = m_DMArray[index]->pPanel;
			DUIRichEdit* pEdit = pPanel->FindChildByNameT<DUIRichEdit>(RESLIB_EDIT); DMASSERT(pEdit);
			if (pEdit && pEdit->DM_IsVisible())
			{
				pEdit->DM_OnKillFocus();
			}
		}
		return DM_ECODE_OK;
	}

	DMCode EDListBoxEx::OnSelectChangedEvent(DMEventArgs *pEvt)
	{
		DMEventLBSelChangedArgs* pSelChangedEvent = (DMEventLBSelChangedArgs*)(pEvt);
		if (pSelChangedEvent && g_pMainWnd)
		{
			g_pMainWnd->OnZiyuankuListboxSeleChanged();
		}
		return DM_ECODE_OK;
	}

	DM::DMCode EDListBoxEx::HandleResLibMenu(int nID)
	{
		switch (nID)
		{
		case RESLIBLISTBOX_RENAME:
		{
			KillAllVisibleEditFocus();

			DMSmartPtrT<DUIItemPanel>& pPanel = m_DMArray[GetCurSel() < 0 ? 0 : GetCurSel()]->pPanel;
			DUIStatic* pStatic = pPanel->FindChildByNameT<DUIStatic>(RESLIB_STATIC); DMASSERT(pStatic);
			DUIRichEdit* pEdit = pPanel->FindChildByNameT<DUIRichEdit>(RESLIB_EDIT); DMASSERT(pEdit);
			if (pEdit && pStatic)
			{
				pEdit->SetAttribute(L"text", pStatic->m_pDUIXmlInfo->m_strText);
				pStatic->SetAttribute(L"bvisible", L"0");
				pEdit->SetAttribute(L"bvisible", L"1");
				
				pEdit->m_EventMgr.SubscribeEvent(DM::DMEventRENotifyArgs::EventID, Subscriber(&EDListBoxEx::OnReslibEditKillFocus, this));
				pEdit->m_EventMgr.SubscribeEvent(DM::DMEventREWantReturnArgs::EventID, Subscriber(&EDListBoxEx::OnReslibEditInputReturn, this));
			}
		}
		break;
		case RESLIBLISTBOX_DELETE:
		{
			DMSmartPtrT<DUIItemPanel>& pPanel = m_DMArray[GetCurSel()]->pPanel;
			DUIStatic* pStatic = pPanel->FindChildByNameT<DUIStatic>(RESLIB_STATIC); DMASSERT(pStatic);

			EDResourceNodeParser* pSeleResourceParser = (EDResourceNodeParser*)GetItemData(GetCurSel());
			if (pSeleResourceParser && pSeleResourceParser->m_pResourceParserOwner)
			{
				ED_MessageBox(L"资源目前被占用中，不能执行删除动作！", MB_OK, L"提示", g_pMainWnd->m_hWnd);
			}
			else
			{
				DeleteItem(GetCurSel());
				pSeleResourceParser->DestoryParser();
			}
		}
		break;
		}

		return DM_ECODE_OK;
	}

	DM::DMCode EDListBoxEx::OnReslibEditKillFocus(DMEventArgs* pEvent)
	{
		DMCode iErr = DM_ECODE_FAIL;
		do
		{
			DMEventRENotifyArgs *pEvt = (DMEventRENotifyArgs*)pEvent;
			if (!pEvt)
				break;
			static bool bRetry = false;
			bool bHaveRenamedEdit = false;
			if (EN_KILLFOCUS == pEvt->m_iNotify && false == bRetry)
			{
				bRetry = true;
				DUIRichEdit* pReslibEdit = (DUIRichEdit*)pEvt->m_pSender;
				CStringW strNewName = pReslibEdit->GetWindowText();
				strNewName.Trim();
				
				DUIWindow* pOwnerWnd = pReslibEdit->DM_GetWindow(GDW_PARENT);
				DUIItemPanel* pPanel = dynamic_cast<DUIItemPanel*>(pOwnerWnd);
				if (!pPanel)
					break;
				DUIStatic* pStatic = pPanel->FindChildByNameT<DUIStatic>(RESLIB_STATIC); DMASSERT(pStatic);
				if (!strNewName.IsEmpty() && pStatic)
				{
					CStringW strOldName = pStatic->m_pDUIXmlInfo->m_strText;
					if (strNewName.Compare((LPCTSTR)strOldName) != 0)
					{						
						int iSeleItem = GetCurSel();
						if (iSeleItem >= 0)
						{
							LPARAM lpData = GetItemData(iSeleItem);
							if (lpData)
							{
								EDResourceNodeParser* pSeleResourceParser = (EDResourceNodeParser*)lpData;
								CStringW strInvalidOutTip;
								if (!EDResourceParser::IsResourceNameCharacterValid(strNewName, pSeleResourceParser->m_strResTag, pSeleResourceParser->m_iFrameCount > 1, strInvalidOutTip))
								{
									ED_MessageBox(strInvalidOutTip, MB_OK, L"重命名错误", g_pMainWnd->m_hWnd);
									pReslibEdit->SetWindowText(strOldName);
									pStatic->DV_SetWindowText(strOldName);
								}
								else
								{
									if (EDResourceParser::sta_pResourceParser && EDResourceParser::sta_pResourceParser->IsResourceNameSameConflict(pSeleResourceParser, strNewName))
									{
										ED_MessageBox(L"同类型资源发生重名", MB_OK, L"重命名错误", g_pMainWnd->m_hWnd);
										pReslibEdit->SetWindowText(strOldName);
										pStatic->DV_SetWindowText(strOldName);
									}
									else
									{
										pStatic->DV_SetWindowText(strNewName);
										pSeleResourceParser->RenameResource(strNewName);
									}
								}
							}
						}
					}
				}

				if (!bHaveRenamedEdit)
				{
					pReslibEdit->DM_SetVisible(false, true);// 此函数会引发多次进入EN_KILLFOCUS，所以加判断
					if (pStatic)
					{
						pStatic->DM_SetVisible(true, true);
					}
				}
				bRetry = false;
				iErr = DM_ECODE_OK;
			}
		} while (false);
		return iErr;
	}

	void EDListBoxEx::OnLButtonDbClick(UINT nFlags, CPoint pt)
	{
		HandleResLibMenu(RESLIBLISTBOX_RENAME);
		SetMsgHandled(FALSE);
	}

	DMCode EDListBoxEx::OnReslibEditInputReturn(DMEventArgs* pEvent)
	{
		DMSmartPtrT<DUIItemPanel>& pPanel = m_DMArray[GetCurSel()]->pPanel;
		DUIRichEdit* pEdit = pPanel->FindChildByNameT<DUIRichEdit>(RESLIB_EDIT); DMASSERT(pEdit);
		if (pEdit) pEdit->DM_OnKillFocus();
		return DM_ECODE_OK;
	}

	int EDListBoxEx::GetText(int nIndex, CStringW& strText)
	{
		if (nIndex < 0 || nIndex >= (int)GetCount())
			return LB_ERR;

		DMSmartPtrT<DUIItemPanel>& pPanel = m_DMArray[nIndex]->pPanel;
		DUIStatic* pStatic = pPanel->FindChildByNameT<DUIStatic>(RESLIB_STATIC); DMASSERT(pStatic);
		strText = pStatic->m_pDUIXmlInfo->m_strText;
		return strText.GetLength();
	}
}

