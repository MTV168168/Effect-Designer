#include "StdAfx.h"
#include "DUIRoot.h"

CPoint DUIRoot::m_pAddParentPt;
DUIRoot::DUIRoot()
{
	m_szWndInitSizeX = 0;
	m_pParent  = NULL;
	m_hRoot    = NULL;
	m_pObjTree = g_pMainWnd->FindChildByNameT<ED::DUIEffectTreeCtrl>(EFFECTTREENAME); DMASSERT(m_pObjTree);
	m_bDown = false;
	m_pPreHoverWnd = NULL;
	m_pCurSeleDUIWnd = NULL;
}

DMCode DUIRoot::InitDesignEditor(HDMTREEITEM hRootTree)
{
	DMCode iErr = DM_ECODE_FAIL;
	do 
	{
// 		CStringW strXml = L"<dm initsize=\"%d, %d\" minsize=\"200, 250\" bresize=\"1\" btranslucent=\"1\">\
// 								<root skin=\"ds_editorbg\" />\
// 							</dm>";

		DMXmlDocument Doc;
		DMXmlNode XmlNode = Doc.Base();
		DMXmlNode XmlDmNode = XmlNode.InsertChildNode(L"dm");
		CStringW strInitSize;
		strInitSize.Format(L"%d, %d", PNGWHOLESIZEX, PNGWHOLESIZEY);
		XmlDmNode.SetAttribute(L"initsize", strInitSize);
		XmlDmNode.SetAttribute(L"minsize", L"200, 250");
		XmlDmNode.SetAttribute(L"btranslucent", L"1");
		DMXmlNode XmlRootNode = XmlDmNode.InsertChildNode(L"root");
		XmlRootNode.SetAttribute(L"skin", L"ds_editorbg");
		m_XmlNode = XmlNode.FirstChild();

		//1. 在OnAttributeFinished中设置了大小
		DMBase::InitDMData(m_XmlNode);

		//2.解析子控件
		InitDMData(m_XmlNode.FirstChild(DUIROOT_NODE));
		
		m_hRoot = hRootTree;
		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

DMCode DUIRoot::SetDesignMode(DesignMode ds_mode)
{
	m_DesignMod = ds_mode;
	return DM_ECODE_OK;
}

void DUIRoot::OnLButtonDown(UINT nFlags,CPoint pt)
{
	DM_SetCapture();
	if (FixedMode == m_DesignMod)
	{
		m_bDown = true;
		m_StartDragPt = pt;
		m_TrackDragPt = m_StartDragPt;
		m_StartDragRc = m_rcWindow;
	}
 	else if (SelectMode == m_DesignMod)  //放到OnLButtonUp
 	{
 		DUIWND hDUIHoverWnd = __super::HitTestPoint(pt,true);
 		DUIWindow* pHoverWnd = g_pDMApp->FindDUIWnd(hDUIHoverWnd);
		if (NULL == SelOrHoverTreeItemByDUIWnd(m_hRoot, pHoverWnd, true))
		{
			m_pObjTree->SelectItem(NULL);//2019-5-11 add
		}
 	}
}

void DUIRoot::OnLButtonUp(UINT nFlags,CPoint pt)
{
	if (SelectMode == m_DesignMod)
	{
		DUIWND hDUIHoverWnd = __super::HitTestPoint(pt, true);
		DUIWindow* pHoverWnd = g_pDMApp->FindDUIWnd(hDUIHoverWnd);
		if (NULL != SelOrHoverTreeItemByDUIWnd(m_hRoot, pHoverWnd, true))
		{
			m_pParent->m_pDragFrame->DV_SetFocusWnd();
		}
	}

	m_bDown = false;
	m_pAddParentPt.SetPoint(0,0);
	DM_ReleaseCapture();
} 

void DUIRoot::OnRButtonDown(UINT nFlags, CPoint pt)
{
	bool bRet = false;
	do 
	{
		if (NULL == m_pObjTree || !m_pObjTree->m_hHoverItem)
		{
			break;
		}

		m_pObjTree->SelectItem(m_pObjTree->m_hHoverItem);
		g_pMainWnd->PopEditorElementMenu(m_pObjTree->m_hHoverItem);
		bRet = true;
	} while (false);
	SetMsgHandled(true == bRet);
}

DUIWND DUIRoot::HitTestPoint(CPoint pt,bool bFindNoMsg)
{
	if (FixedMode != m_DesignMod)
	{
		DUIWND hDUIHoverWnd = __super::HitTestPoint(pt,true);
		DUIWindow* pHoverWnd = g_pDMApp->FindDUIWnd(hDUIHoverWnd);
		if (pHoverWnd && m_pPreHoverWnd != pHoverWnd && m_pParent)
		{
			if (pHoverWnd != m_pParent->m_pShow && // 去掉主窗口的hover状态
				m_pCurSeleDUIWnd != pHoverWnd) // 当前选中窗口   不hover
			{
				m_pParent->HoverInSelMode(pHoverWnd);
				SelOrHoverTreeItemByDUIWnd(m_hRoot, pHoverWnd, false);
			}
			else if (m_pCurSeleDUIWnd == pHoverWnd) //hove状态
			{
				m_pObjTree->HoverItem(m_pObjTree->GetSelectedItem(), false);
			}
			else
			{
				m_pParent->HideHoverFrameWnd();
			}
		}
		m_pPreHoverWnd = pHoverWnd;
	}

	return m_hDUIWnd;
}

DMCode DUIRoot::AlignDragFrame(const CRect& rtWnd)
{
#define FRAMERANGE 1
	bool bFindWndOk = false;
	DUIWindow* pChild = m_Node.m_pLastChild;
	while (pChild)
	{
		if (pChild->DM_IsVisible(true) && !pChild->DM_IsMsgNoHandle())
		{
			if (rtWnd != pChild->m_rcWindow) //不是自身窗口
			{
				if (rtWnd.left <= pChild->m_rcWindow.left + FRAMERANGE && rtWnd.left >= pChild->m_rcWindow.left - FRAMERANGE
					|| rtWnd.right <= pChild->m_rcWindow.left + FRAMERANGE && rtWnd.right >= pChild->m_rcWindow.left - FRAMERANGE)//left align
				{
					m_pParent->AlignMentFrameInSelMode(pChild, AlignmentLeft);
					bFindWndOk = true;
					break;
				}
				else if (rtWnd.left <= pChild->m_rcWindow.right + FRAMERANGE && rtWnd.left >= pChild->m_rcWindow.right - FRAMERANGE
					|| rtWnd.right <= pChild->m_rcWindow.right + FRAMERANGE && rtWnd.right >= pChild->m_rcWindow.right - FRAMERANGE) //right align
				{
					m_pParent->AlignMentFrameInSelMode(pChild, AlignmentRight);
					bFindWndOk = true;
					break;
				}
				else if (rtWnd.top <= pChild->m_rcWindow.top + FRAMERANGE && rtWnd.top >= pChild->m_rcWindow.top - FRAMERANGE
					|| rtWnd.bottom <= pChild->m_rcWindow.top + FRAMERANGE && rtWnd.bottom >= pChild->m_rcWindow.top - FRAMERANGE) //top align
				{
					m_pParent->AlignMentFrameInSelMode(pChild, AlignmentTop);
					bFindWndOk = true;
					break;
				}
				else if (rtWnd.bottom <= pChild->m_rcWindow.bottom + FRAMERANGE && rtWnd.bottom >= pChild->m_rcWindow.bottom - FRAMERANGE
					|| rtWnd.top <= pChild->m_rcWindow.bottom + FRAMERANGE && rtWnd.top >= pChild->m_rcWindow.bottom - FRAMERANGE) //botton align
				{
					m_pParent->AlignMentFrameInSelMode(pChild, AlignmentBottom);
					bFindWndOk = true;
					break;
				}
			}
		}
		pChild = pChild->m_Node.m_pPrevSibling;
	}

	if (!bFindWndOk)
		m_pParent->HideAlignMentFrameWnd();

	return DM_ECODE_OK;
}

HDMTREEITEM DUIRoot::SelOrHoverTreeItemByDUIWnd(HDMTREEITEM hRoot,DUIWindow* pDUIWnd,bool bSel)
{
	HDMTREEITEM hTreeItem = NULL;
	do 
	{
		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pObjTree->GetItemData(hRoot);
		if (pData && pData->m_pRootWnd == pDUIWnd) //lzlong  不选中根节点
		{
			break;
		}
		if (pData && pData->m_pDUIWnd == pDUIWnd)
		{
			hTreeItem = hRoot;
			break;
		}

		HDMTREEITEM hChild = m_pObjTree->GetChildItem(hRoot);
		while (hChild)
		{
			HDMTREEITEM hTemp = SelOrHoverTreeItemByDUIWnd(hChild,pDUIWnd,bSel);
			if (hTemp)
			{
				hTreeItem = hTemp;
				break;
			}
			hChild = m_pObjTree->GetNextSiblingItem(hChild);
		}

	} while (false);
	if (hTreeItem)
	{
		if (bSel)
		{
			m_pObjTree->SelectItem(hTreeItem);
		}
		else
		{
			m_pObjTree->HoverItem(hTreeItem,false);
		}
	}
	
	return hTreeItem;
}

bool DUIRoot::MLDownInSelMode(CPoint pt,DUIWindow* pCurSelWnd)
{
	bool bRet = false;
	do 
	{
		DUIWND hDUIHoverWnd = __super::HitTestPoint(pt,true);
		DUIWindow* pHoverWnd = g_pDMApp->FindDUIWnd(hDUIHoverWnd);
		if (NULL == pHoverWnd)// 相等或未找到返回false,dragframe绘制它的内边框
		{
			break;
		}
		if (pCurSelWnd == pHoverWnd && m_pObjTree->GetSelectedItem() != NULL)
		{
			break;
		}
		if (pHoverWnd&&m_pParent)
		{
			HDMTREEITEM hSel = SelOrHoverTreeItemByDUIWnd(m_hRoot,pHoverWnd,true);
		}
		bRet = true;
	} while (false);
	return bRet;
}

DUIWindow* DUIRoot::CreateAddChild(DUIWindow* pParentWnd,CStringW strReg)
{
	DUIWindow *pChild = NULL;
	do 
	{
		//直接创建对象
		g_pDMApp->CreateRegObj((void**)&pChild,strReg,DMREG_Window);
		if (NULL == pChild)
		{ 
			break;
		}

		pParentWnd->DM_InsertChild(pChild);
	} while (false);
	return pChild;
}

bool DUIRoot::InitAddChild(ObjTreeData* pParentData, DUIWindow* pWnd, EDJSonParserPtr pJSonParser)
{
	bool bRet = false;
	do
	{
		if (NULL == pParentData || NULL == pParentData->m_pDUIWnd || NULL == pWnd)
		{
			break;
		}

		CRect rcMeasure; INT iWidth = 0, iHeight = 0;
		pParentData->m_pDUIWnd->DV_GetChildMeasureLayout(rcMeasure);
		m_pAddParentPt = rcMeasure.TopLeft();
		
		DMASSERT(pJSonParser);
		CPoint ptTopLeft = pJSonParser->GetParserItemTopLeftPt();
		pJSonParser->GetRelativeResourceImgSize(iWidth, iHeight);
		double scale = pJSonParser->GetParserImageScale();

		CStringW strPos = L"0,0,-0,-0";
		strPos.Format(L"%d,%d,%d,%d", ptTopLeft.x + ROOTPNGXOFFSET, ptTopLeft.y + ROOTPNGYOFFSET, ptTopLeft.x + ROOTPNGXOFFSET + (int)(iWidth * scale), ptTopLeft.y + ROOTPNGYOFFSET + (int)(iHeight * scale));

		DMXmlDocument Doc;
		DMXmlNode XmlNode = Doc.Base();
		XmlNode = XmlNode.InsertChildNode(L"Window");
		XmlNode.SetAttribute(L"skin", DEFAULTPREVIEWSKIN);
		XmlNode.SetAttribute(L"pos", strPos, false);
		if (!pParentData)
			break;
		pWnd->InitDMData(XmlNode);
		bRet = true;
	} while (false);
	return bRet;
}

DUIWindow* DUIRoot::GetAddChild(HDMTREEITEM parent, EDJSonParserPtr pJSonParser)
{
	DUIWindow* pAdd = NULL;
	do 
	{
		HDMTREEITEM hItem = parent;
		if (NULL == hItem || DMTVI_ROOT == hItem)
		{
			hItem = m_pObjTree->GetRootItem();
			if (NULL == hItem || DMTVI_ROOT == hItem)
				break;
		}

		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pObjTree->GetItemData(hItem); DMASSERT(pData);
		if (!pData)
			break;
		DUIWindow* pRootWnd = pData->m_pRootWnd;
		if (!pRootWnd)
			break;

		//1. 创建新窗口
		DUIWindow *pChild = CreateAddChild(pRootWnd, L"window");
		if (NULL == pChild)
		{ 
			break;
		}

		//2. 初始化通用属性
		if (!InitAddChild(pData, pChild, pJSonParser))
			break;

		pAdd = pChild;
	} while (false);
	return pAdd;
}

DMCode DUIRoot::OnAttributeFinished(LPCWSTR pszAttribute,LPCWSTR pszValue,bool bLoadXml,DMCode iErr)
{
	// DMHWnd的属性，用于模拟支持HostAttr的SetAttribute
	if (0 == dm_wcsicmp(pszAttribute, DMAttr::DMHWndAttr::BYTE_alpha)
		||0 == dm_wcsicmp(pszAttribute, DMAttr::DMHWndAttr::SIZE_initsize)
		)
	{
		if (0 == dm_wcsicmp(pszAttribute, DMAttr::DMHWndAttr::SIZE_initsize))
		{
			CSize szInit;
			dm_parsesize(pszValue,szInit);
			CRect rcWnd(0,0,szInit.cx,szInit.cy);
			m_szWndInitSizeX = szInit.cx;
			rcWnd.OffsetRect(-m_szWndInitSizeX / 2 + m_pParent->m_rcWindow.left + m_pParent->m_rcWindow.Width() / 2, m_pParent->m_rcWindow.top - ROOTPNGYOFFSET + ROOTWNDYOFFSET);
			
			DM_FloatLayout(rcWnd);
			m_pParent->HoverInSelMode(this);
			m_pParent->DragFrameInSelMode();
			m_pParent->ReferenceLineFrameInSelMode();
			m_pParent->DM_Invalidate();
		}

		if (0 == dm_wcsicmp(pszAttribute, DMAttr::DMHWndAttr::BYTE_alpha))
		{// 利用DUI的alpha来模拟Res中主窗口的alpha
			m_pDUIXmlInfo->m_pStyle->SetAttribute(DMAttr::DMHWndAttr::BYTE_alpha,pszValue,bLoadXml);
			if (!bLoadXml)
			{
				DM_Invalidate();
			}
		}

		iErr = DM_ECODE_OK;
	}
	return __super::OnAttributeFinished(pszAttribute,pszValue,bLoadXml,iErr);
}



