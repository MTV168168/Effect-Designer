#include "StdAfx.h"
#include "EDBackgroundEdge.h"

EDBackgroundEdgeParser::EDBackgroundEdgeParser()
	: m_iColor(-1)
	, m_bEnable(true)
	, m_iWidth(5)
	, m_iZPosition(-1)
{
}

EDBackgroundEdgeParser::~EDBackgroundEdgeParser()
{
}

DM::DMCode EDBackgroundEdgeParser::SetJSonAttribute(LPCSTR pszAttribute, JSHandle& JsHandleValue, bool bLoadJSon)
{
	DMCode iErr = DM_ECODE_FAIL;

	if (0 == _stricmp(pszAttribute, EDAttr::EDBackgroundEdgeParserAtrr::INT_color))
	{
		if (JsHandleValue.isInt())
		{
			m_iColor = JsHandleValue.toInt();
			iErr = DM_ECODE_OK;
		}
	}
	else if (0 == _stricmp(pszAttribute, EDAttr::EDBackgroundEdgeParserAtrr::BOOL_enable))
	{
		if (JsHandleValue.isBool())
		{
			m_bEnable = JsHandleValue.toBool();
			iErr = DM_ECODE_OK;
		}
	}
	else if (0 == _stricmp(pszAttribute, EDAttr::EDBackgroundEdgeParserAtrr::INT_width))
	{
		if (JsHandleValue.isInt())
		{
			m_iWidth = JsHandleValue.toInt();
			iErr = DM_ECODE_OK;
		}
	}
	else if (0 == _stricmp(pszAttribute, EDAttr::EDBackgroundEdgeParserAtrr::INT_zposition))
	{
		if (JsHandleValue.isInt())
		{
			m_iZPosition = JsHandleValue.toInt();
			iErr = DM_ECODE_OK;
		}
	}
	else
	{//强制设置为OK 避免后面可能new出其他节点出来   因为已经不可能有子节点了
		DMASSERT_EXPR(false, L"Beautify 有没有解析的节点");
		iErr = DM_ECODE_NOLOOP;
	}
	if (!bLoadJSon)
		MarkDataDirty();
	return iErr;
}

DM::DMCode EDBackgroundEdgeParser::BuildJSonData(JSHandle JSonHandler)
{
	if (m_strJSonMemberKey.IsEmpty())
	{
		m_strJSonMemberKey = std::string(EDBASE::w2a(GetClassNameW())).c_str();
	}
	JSHandle JSonChild = JSonHandler[(LPCSTR)m_strJSonMemberKey];
	return EDJSonParser::BuildJSonData(JSonChild);
}

DM::DMCode EDBackgroundEdgeParser::BuildMemberJsonData(JSHandle &JSonHandler)
{
	JSonHandler[EDAttr::EDBackgroundEdgeParserAtrr::INT_color].putInt(m_iColor);
	JSonHandler[EDAttr::EDBackgroundEdgeParserAtrr::BOOL_enable].putBool(m_bEnable);
	JSonHandler[EDAttr::EDBackgroundEdgeParserAtrr::INT_width].putInt(m_iWidth);
	JSonHandler[EDAttr::EDBackgroundEdgeParserAtrr::INT_zposition].putInt(m_iZPosition);
	return DM_ECODE_FAIL;
}

int EDBackgroundEdgeParser::GetZPositionOrder()
{
	return m_iZPosition;
}

bool EDBackgroundEdgeParser::SetZPositionOrder(int iZPostion)
{
	m_iZPosition = iZPostion;
	return true;
}

bool EDBackgroundEdgeParser::IsParserEnable()
{
	return m_bEnable;
}