#include "stdafx.h"
#include "EDBase.h"

// namespace DM
// {
	// EDDataBase------------------------------------------
	DMCode EDDataBase::InitJSonData(JSHandle &JSonHandler)
	{
		DMCode iErr = DM_ECODE_OK;
		do 
		{
			if (false == JSonHandler.isValid())
			{
				iErr = 200;
				break;
			}

			std::vector<std::string> numbers = JSonHandler.memberNames();
			if (!numbers.empty())
			{
				for (std::string& item : numbers)
				{
					ParseMemberJSObj(item.c_str(), JSonHandler[item.c_str()], true);
				}
			}
		} while (false);
		return iErr;
	}

	DMCode EDDataBase::SetJSonAttribute(LPCSTR pszAttribute, JSHandle& pszValue, bool bLoadJSon)
	{
		return DefAttributeProc(pszAttribute, pszValue, bLoadJSon);
	}

	DMCode EDDataBase::DefAttributeProc(LPCSTR pszAttribute, JSHandle& pszValue, bool bLoadJSon)
	{
		return DM_ECODE_FAIL;//未解析
	}

	DMCode EDDataBase::ParseMemberJSObj(LPCSTR pszAttribute, JSHandle& JSonHandler, bool bLoadJSon)
	{
		return DM_ECODE_FAIL;//未解析
	}

	DMCode EDDataBase::ParseJSNodeObjFinished()
	{
		return DM_ECODE_FAIL;//未解析
	}

	DMCode EDDataBase::SetJSonMemberKey(LPCSTR lpKeyVal)
	{
		return DM_ECODE_NOTIMPL;//未解析
	}

	DMCode EDDataBase::BuildJSonData(JSHandle JSonHandler)
	{
		return DM_ECODE_NOTIMPL;//未生成
	}

	DMCode EDDataBase::BuildMemberJsonData(JSHandle &JSonHandler)
	{
		return DM_ECODE_NOTIMPL;//未生成
	}

	// EDBase------------------------------------------
	LPCWSTR EDBase::GetClassName()
	{
		return NULL;
	}

	LPCWSTR EDBase::V_GetClassName()
	{
		return NULL;
	}

	LPCWSTR EDBase::GetBaseClassName()
	{
		return NULL;
	}

	int EDBase::GetClassType()
	{
		return DMREG_Unknown;
	}

	int EDBase::V_GetClassType()
	{
		return DMREG_Unknown;
	}

	bool EDBase::IsClass(LPCWSTR lpszName)
	{
		return false;
	}

	namespace EDAttr
	{
		// ----------------------------------------------------
		// 内部强制规定属性字符串定义方式
		char* EDInitAttrDispatch::GetAttrValue(char* cls, char *pBuf)
		{
			char* pLow = strstr(pBuf, "_");
			if (!pLow){ DMASSERT_EXPR(0, L"请使用类型_定义方式,如INT_ihei"); }
			pLow++;
			return pLow;
		}
	}

//}//namespace DM