#include "stdafx.h"
#include "FileHelper.h"

namespace FileHelper
{
	BOOL MyReadFile(LPCTSTR szFileName, std::string & szBuffer)
	{
		szBuffer.clear();
		HANDLE hFile = CreateFile(szFileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if (hFile == INVALID_HANDLE_VALUE)
		{
			return FALSE;
		}

		DWORD nFileSize = GetFileSize(hFile, NULL);
		char * pBuffer = new char[nFileSize + 1]; // 最后一位为 '/0',C-Style 字符串的结束符。
		DWORD nReadSize = 0;
		if (!ReadFile(hFile, pBuffer, nFileSize, &nReadSize, NULL))
		{
			delete[] pBuffer;
			CloseHandle(hFile);
			return FALSE;
		}

		if (nReadSize <= 0)
		{
			CloseHandle(hFile);
			delete[] pBuffer;
			return FALSE;
		}

		pBuffer[nReadSize] = '\0';
		CloseHandle(hFile);
		szBuffer = pBuffer;
		delete[] pBuffer;

		return TRUE;
	}


	BOOL MyWriteFile(LPCTSTR szFile, const char * pBuffer, int nSize)
	{
		HANDLE hFile = CreateFile(szFile,GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		if (hFile == INVALID_HANDLE_VALUE)
		{
			DWORD dwErr = GetLastError();
			return FALSE;
		}

		if (pBuffer == NULL || nSize == 0)
		{
			return TRUE;
		}

		DWORD nWriteSize;
		BOOL bRet = ::WriteFile(hFile, pBuffer, nSize, &nWriteSize, NULL);
		CloseHandle(hFile);

		return bRet;
	}

	//判断目录是否为空
	BOOL IsDirectoryEmpty(LPCWSTR dir)
	{
		TCHAR szFind[MAX_PATH];
		lstrcpy(szFind,dir);
		lstrcat(szFind, L"\\*.*");

		WIN32_FIND_DATA wfd;
		HANDLE hFind = FindFirstFile(szFind, &wfd);

		if (hFind == INVALID_HANDLE_VALUE) // 如果没有找到或查找失败
			return TRUE;

		do
		{
			if((0 == lstrcmp(wfd.cFileName , L".")) || ( 0 == lstrcmp(wfd.cFileName , L"..")));
			else
				return FALSE;
		}while (FindNextFile(hFind,&wfd));

		FindClose(hFind);
		return TRUE;
	}

	bool CreateMultipleDirectory(std::wstring path)
	{
		if ((path[path.size() - 1] != L'\\') && (path[path.size() - 1] != L'/'))
		{
			path = path + L"\\";
		}

		if (PathIsDirectory(path.c_str()) == FILE_ATTRIBUTE_DIRECTORY)
		{
			return true;
		}

		std::wstring strDir = path;//存放要创建的目录字符串
		std::vector<std::wstring> vPath;//存放每一层目录字符串
		std::wstring strTemp;//一个临时变量,存放目录字符串
		bool bSuccess = false;//成功标志
		std::wstring::const_iterator sIter;//定义字符串迭代器
		//遍历要创建的字符串
		for (sIter = strDir.begin(); sIter != strDir.end(); sIter++) {
			if ((*sIter != '\\') && (*sIter != '/')) {//如果当前字符不是'\\','/'
				strTemp += (*sIter);
			}
			else {//如果当前字符是'\\'
				vPath.push_back(strTemp);//将当前层的字符串添加到数组中
				strTemp += '\\';
			}
		}

		//遍历存放目录的数组,创建每层目录
		std::vector<std::wstring>::const_iterator vIter;
		for (vIter = vPath.begin(); vIter != vPath.end(); ++vIter)
		{
			bSuccess = CreateDirectory((*vIter).c_str(), NULL) ? true : false;
		}

		return bSuccess;
	}

}