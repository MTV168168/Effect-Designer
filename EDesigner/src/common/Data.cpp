#include "stdAfx.h"
#include "Data.h"

// ObjTreeData
ObjTreeData::ObjTreeData(DUIRoot* pRootWnd, DUIWindowPtr pDUI, EffectTempletType iEffectType, HDMTREEITEM hTreeItem, EDJSonParserPtr pJSonParser /*= NULL*/)
{
	memset(m_bAttrlistExp, 1, sizeof(m_bAttrlistExp));
	SetData(pRootWnd, pDUI, iEffectType, hTreeItem, pJSonParser);
}

ObjTreeData::~ObjTreeData()
{
	m_pRootWnd = NULL;
	m_pDUIWnd  = NULL;
	m_iEffectType = EFFECTTEMPBTN_BASE;
	m_pJsonParser = NULL;
}

void ObjTreeData::SetData(DUIRoot* pRootWnd, DUIWindowPtr pDUI, EffectTempletType iEffectType, HDMTREEITEM hTreeItem, EDJSonParserPtr pJSonParser /*= NULL*/)
{
	m_pRootWnd = pRootWnd;
	m_pDUIWnd = pDUI;
	m_iEffectType = iEffectType;
	m_pJsonParser = pJSonParser;
	if (m_pJsonParser)
	{
		m_pJsonParser->m_hTreeBindItem = hTreeItem;
	}
}

bool ObjTreeData::IsValid() const
{
	return true;
}

TransitionTreeData::TransitionTreeData(int tag, EDJSonParserPtr pJSonParser /*= NULL*/)
{
	m_pJSonParser = pJSonParser;
	m_treeTag = tag;
}