// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	ActionSlot.h
// File mark:   
// File summary:
// Author:		lzlong
// Edition:     1.0
// Create date: 2019-3-20
// ----------------------------------------------------------------
#pragma once

#include <memory>

enum ActionSlotType
{
	ActionSlotTypeBase = -1,
	NullSlot,
	TreeSelectItemSlot,
	TreeDeleteItemSlot,
	TreeAddItemSlot,
	ComboSeleChangeSlot,
	EditorElemPosChangeSlot,
};

class ActionSlot
{
public:
	ActionSlot();
	virtual ~ActionSlot();

	virtual DMCode PerformUndoActionSlot() = 0;
	virtual DMCode PerformRedoActionSlot() = 0;
	bool   IsActExCuteAble()
	{
		return (GetSlotID() != NullSlot);
	}
	virtual UINT GetSlotID() = 0;

private:
};

//---------------------------------------------------
// Function Des: 空动作类型  有些东西需要空动作来补充 
//---------------------------------------------------
class NullActionSlot : public ActionSlot
{
public:
	NullActionSlot();
	~NullActionSlot();

	virtual DMCode PerformUndoActionSlot();
	virtual DMCode PerformRedoActionSlot();
	virtual UINT GetSlotID()
	{
		return NullSlot;
	}
};

//---------------------------------------------------
// Function Des: 树的动作描述
//---------------------------------------------------

namespace ED
{
	class DUIEffectTreeCtrl;
}

//---------------------------------------------------
// Function Des: 树的选项改变动作描述
//---------------------------------------------------
class TreeSelectActionSlot : public ActionSlot
{
public:
	TreeSelectActionSlot(HDMTREEITEM hOldItem, HDMTREEITEM hNewItem, ED::DUIEffectTreeCtrl*);
	~TreeSelectActionSlot();

	virtual UINT GetSlotID()
	{
		return TreeSelectItemSlot;
	}
	virtual DMCode PerformUndoActionSlot();
	virtual DMCode PerformRedoActionSlot();
private:
	ED::DUIEffectTreeCtrl*	m_pTreeCtrl;
	HDMTREEITEM				m_hOldItem;
	DM::LPTVITEMEX			m_pTreeOldDataLaw;
	LPARAM					m_pTreeOldlParam;
	HDMTREEITEM				m_hNewItem;
	DM::LPTVITEMEX			m_pTreeNewDataLaw;
	LPARAM					m_pTreeNewlParam;
};

//---------------------------------------------------
// Function Des: 树的删除动作描述
//---------------------------------------------------
class TreeDeleteActionSlot : public ActionSlot
{
public:
	TreeDeleteActionSlot(HDMTREEITEM hItem, ED::DUIEffectTreeCtrl*);
	~TreeDeleteActionSlot();

	virtual UINT GetSlotID()
	{
		return TreeDeleteItemSlot;
	}
	virtual DMCode PerformUndoActionSlot();
	virtual DMCode PerformRedoActionSlot();

private:

	ED::DUIEffectTreeCtrl*			m_pTreeCtrl;
	bool					m_bDeleteParentItem; ///<删除的是父节点项
	HDMTREEITEM				m_hParentItem;
	DM::LPTVITEMEX			m_pTreeParentDataLaw;
	LPARAM					m_pTreeParentlParam;
	HDMTREEITEM				m_hChildItem;
	DM::LPTVITEMEX			m_pTreeChildDataLaw;
	LPARAM					m_pTreeChildlParam;
};

//---------------------------------------------------
// Function Des: 树的添加项动作描述
//---------------------------------------------------
class TreeAddActionSlot : public ActionSlot
{
public:
	TreeAddActionSlot(HDMTREEITEM hItem, ED::DUIEffectTreeCtrl*);
	~TreeAddActionSlot();

	virtual UINT GetSlotID()
	{
		return TreeAddItemSlot;
	}
	virtual DMCode PerformUndoActionSlot();
	virtual DMCode PerformRedoActionSlot();

private:

	ED::DUIEffectTreeCtrl*			m_pTreeCtrl;
	bool					m_bAddParentItem; ///<添加的是父节点项
	HDMTREEITEM				m_hParentItem;
	DM::LPTVITEMEX			m_pTreeParentDataLaw;
	LPARAM					m_pTreeParentParam;
	HDMTREEITEM				m_hChildItem;
	DM::LPTVITEMEX			m_pTreeChildDataLaw;
	LPARAM					m_pTreeChildlParam;
};

//---------------------------------------------------
// Function Des: Combobox的项改变动作描述
//---------------------------------------------------
class ComboSeleChangeActionSlot : public ActionSlot
{
public:
	ComboSeleChangeActionSlot(HDMTREEITEM hItem, int iOldSelect, int iNewSelect, DUIComboBox*, ED::DUIEffectTreeCtrl*);
	~ComboSeleChangeActionSlot();

	virtual UINT GetSlotID()
	{
		return ComboSeleChangeSlot;
	}
	virtual DMCode PerformUndoActionSlot();
	virtual DMCode PerformRedoActionSlot();
	
private:
	ED::DUIEffectTreeCtrl*	m_pTreeCtrl;
	DUIComboBox*			m_pComboBox;
	int						m_iNewSelect;
	int						m_iOldSelect;
	HDMTREEITEM				m_hItem;
	DM::LPTVITEMEX			m_pTreeDataLaw;
	LPARAM					m_pTreelParam;
};

//---------------------------------------------------
// Function Des: Editor里面的元素位置改变项动作描述
//---------------------------------------------------
class EditorElemPosChgActionSlot : public ActionSlot
{ 
public:
	EditorElemPosChgActionSlot(HDMTREEITEM hItem, CRect OldRect, CRect NewRect, ED::DUIEffectTreeCtrl*, DUIDragFrame*);
	~EditorElemPosChgActionSlot();

	virtual UINT GetSlotID()
	{
		return EditorElemPosChangeSlot;
	}
	virtual DMCode PerformUndoActionSlot();
	virtual DMCode PerformRedoActionSlot();

private:

	DUIDragFrame*			m_pDragFrame;
	ED::DUIEffectTreeCtrl*	m_pTreeCtrl;
	HDMTREEITEM				m_hItem;
	DM::LPTVITEMEX			m_pTreeDataLaw;
	LPARAM					m_pTreeParam;
	CRect					m_OldRect;
	CRect					m_NewRect;
};