// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	Events.h
// File mark:   
// File summary: 事件系统整体copy自CEGUI
// Author:		lzlong
// Edition:     1.0
// Create date: 2019-7-23
// ----------------------------------------------------------------
#pragma once

namespace DM
{
	/// <summary>
	///		事件ID
	/// </summary>
	enum EVT_ID
	{
		DMEVT_DRLISTBOX_DBLCLICK = 24400,

		// XmlEditor
		DMEVT_XML_UPDATA = 24500,
	};

	/// <summary>
	///		当双击DUIRecentListBox的项时，发送此消息
	/// </summary>
	class DUIRecentListBoxDBLClickArgs :  public DMEventArgs
	{
	public:
		DUIRecentListBoxDBLClickArgs(DUIWindow *pWnd) :DMEventArgs(pWnd){}
		enum{EventID=DMEVT_DRLISTBOX_DBLCLICK};
		virtual UINT GetEventID(){return EventID;}
		LPCSTR GetEventName(){return EVEIDNAME(DMEVT_DRLISTBOX_DBLCLICK);}
		CStringW                  m_strDir;
	};

}//namespace DM