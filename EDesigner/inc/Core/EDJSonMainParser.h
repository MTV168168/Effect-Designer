//-------------------------------------------------------
// Copyright (c) 
// All rights reserved.
// 
// File Name: EDJSonMainParser.h 
// File Des: Json顶级节点  Json解析类
// File Summary: 
// Cur Version: 1.0
// Author:
// Create Data:
// History:
// 		<Author>	<Time>		<Version>	  <Des>
//      lzlong		2019-5-24	1.0		
//-------------------------------------------------------
#pragma once

namespace EDAttr
{
	class EDJSonMainParserAttr
	{
	public:
		static char* STRING_deformationFileName;				///< 示例:"deformationFileName": "INZHUANG_deformation.json"
		static char* STING_newPartField;						///< 示例:"newPartField": "3.0"
		static char* STRING_stickerType;                        ///< 示例:"stickerType": "faceMorph_faceDeformation"
		static char* STRING_toolVersion;						///< 示例:"toolVersion": "Win_2.2.0",
		static char* DOUBLE_totalMem;							///< 示例:""totalMem": 2.22,
		static char* STRING_version;							///< 示例:"version": "3.0"
	};
	EDAttrValueInit(EDJSonMainParserAttr, STRING_deformationFileName)EDAttrValueInit(EDJSonMainParserAttr, STING_newPartField)EDAttrValueInit(EDJSonMainParserAttr, STRING_stickerType)
	EDAttrValueInit(EDJSonMainParserAttr, STRING_toolVersion)EDAttrValueInit(EDJSonMainParserAttr, DOUBLE_totalMem)EDAttrValueInit(EDJSonMainParserAttr, STRING_version)
}

// namespace DM
// {
	/// <summary>
	///		json文件顶级节点json字段解析类
	/// </summary>
	class EDJSonMainParser : public EDJSonParser
	{
		EDDECLARE_CLASS_NAME(EDJSonMainParser, L"EDJSonMainParser", DMREG_Attribute);
	public:
		EDJSonMainParser(IEDJSonParserOwner* pOwner);
		~EDJSonMainParser();
		void InitData();

		DMCode SetJSonAttribute(LPCSTR pszAttribute, JSHandle& JsHandleValue, bool bLoadJSon) override;
		DMCode BuildMemberJsonData(JSHandle &JSonHandler) override;
		void GenerateStickerTypeInfoValue();
		void FreeParser(EDJSonParser* pParser) override;														///< delete节点

	public:
		IEDJSonParserOwner* m_pOwner;
		CStringW		m_strDeformationFileName;
		CStringW		m_strNewPartField;
		CStringW		m_strToolVersion;
		CStringW		m_strStickerType;
		CStringW		m_strVersion;
		double			m_dbTotalMem;		
	};

//}; //end namespace DM