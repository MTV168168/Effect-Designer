#include "EDBaseAfx.h"
#include "TaskSvc.h"
#include "DMLock.h"
#include <process.h>

Tasks::Tasks()
:m_hTaskWaitEvent(NULL),
m_hTaskFinishEvent(NULL),
m_bExitTaskThread(false),
m_dwThreadId(0),
m_bRunning(false),
m_pThreadLocker(NULL),
m_dwTimeOut(INFINITE)
{

}

bool Tasks::AddNewTask(ITaskPtr pTask, bool bIsExec /*= true*/)
{
	bool bSuccess = false;
	if (pTask)
	{
		{
			DMAutoLock locker(&m_TaskLocker);
			m_TaskWaitList.push_back(pTask);
		}
		bSuccess = bIsExec ? ExecTask() : true;
	}

	return bSuccess;
}

bool Tasks::ExecTask(bool bIsSync /*= false*/, DWORD dwTimeOut /*= INFINITE*/)
{
	bool bRet = false;
	do 
	{
		if (this->m_dwThreadId == ::GetCurrentThreadId()) 
		{
			DMASSERT_EXPR(false,L"任务服务线程不能向自己请求任务服务，否则会死锁！！！");
			break;	
		}
		{// 若是空任务就不再执行
			DMAutoLock locker(&m_TaskLocker);
			if (this->m_TaskWaitList.empty())
			{
				bRet = true;
				break;
			}
		}
		if (m_hTaskWaitEvent)
		{
			if (::SetEvent(m_hTaskWaitEvent))
			{// 激活Wait事件，使_RunTasksThread执行任务队列
				bRet = true;
			}
		}

		if (bIsSync && m_hTaskFinishEvent)
		{
			HRESULT hr = E_FAIL;
			DWORD dwIndex = -1;
			hr = ::CoWaitForMultipleHandles(0,dwTimeOut,1,&m_hTaskFinishEvent,&dwIndex);
			if (S_OK == hr)
			{
				bRet = true;
			}
			else
			{
				bRet = false;
			}
		}
	} while (false);
	return bRet;
}

//protect
bool Tasks::_InitTasksThread()
{
	bool bRet = false;
	do 
	{
		if (!m_pThreadLocker)
		{
			m_pThreadLocker = new DMLock;
		}

		DMAutoLock lockIt(m_pThreadLocker);
		if (!m_hTaskWaitEvent) 
		{
			m_hTaskWaitEvent = ::CreateEvent(NULL, FALSE, FALSE, NULL);
		}

		if (!m_hTaskFinishEvent) 
		{
			m_hTaskFinishEvent = ::CreateEvent(NULL, FALSE, FALSE, NULL);
		}

		if (m_hTaskWaitEvent && m_hTaskFinishEvent) 
		{
			m_bExitTaskThread = false;
			bRet = true;
		}
	} while (false);
	return bRet;
}

DWORD Tasks::_RunTasksThread()
{
	OleInitialize(NULL);// IE需要COM环境
	DWORD dwError = ERROR_SUCCESS;
	m_dwThreadId = ::GetCurrentThreadId();
	m_bRunning = true;
	do 
	{	
		DWORD dwTickStart = ::GetTickCount();
		if (false == _DealWithMessage())
		{//1.WM_QUIT退出消息循环
			break;
		}
		_GetNewTaskList();// 获得任务队列
		_DealWithTaskList();// 执行任务队列
		DWORD dwTimeOut = m_dwTimeOut;
		if (INFINITE != dwTimeOut)
		{
			DWORD dwTick = ::GetTickCount() - dwTickStart;
			if (dwTick > m_dwTimeOut)
			{
				dwTimeOut = 0;
			}
			else
			{
				dwTimeOut -= dwTick;
			}
		}
		dwError = ::MsgWaitForMultipleObjects(1, &m_hTaskWaitEvent, FALSE, dwTimeOut, QS_ALLINPUT);// QS_ALLEVENTS可接受Post消息，An input, WM_TIMER, WM_PAINT, WM_HOTKEY, or posted message is in the queue.
	} while (false == m_bExitTaskThread && WAIT_FAILED != dwError);

	//  Delete task
	t_lstTaskQueue::iterator iter = m_TaskRunList.begin();
	t_lstTaskQueue::iterator iterEnd = m_TaskRunList.end();
	for ( ; iterEnd != iter; ++iter) 
	{
		t_lstTaskQueue::value_type& pTask = (*iter);
		if (pTask)
		{
			pTask->Release();
		}
	}
	m_TaskRunList.clear();

	// 正常退出流程！！！
	// 关闭任务是否完成信号m_hTaskFinishEvent。
	if (m_hTaskFinishEvent)
	{
		::SetEvent(m_hTaskFinishEvent);
	}
	m_bRunning = false;
	return TRUE;
}

bool Tasks::_UnInitTasksThread()
{
	bool bRet = false;
	do 
	{
		m_bExitTaskThread = true;///< 退出线程的另一条件
		if (m_dwThreadId)
		{
			::PostThreadMessage(m_dwThreadId, WM_QUIT, 0, 0);//唤醒_RunTasksThread
			m_dwTimeOut = 1000;///< 为防止消息丢失,加入超时退出
		}

		// 默认等待3秒。
		bRet = ExecTask(true, 3000);
		_DestroyTasks();
		DM_DELETE(m_pThreadLocker);
	} while (false);
	return bRet;
}

bool Tasks::_IsTaskRunning()
{
	return m_bRunning;
}

// private
bool Tasks::_DealWithMessage()
{
	MSG msg;

	while (::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) 
	{ 
		// If it is a quit message, exit.
		if (msg.message == WM_QUIT)  
			return false; 

		::TranslateMessage(&msg);
		::DispatchMessage(&msg);

	} // End of PeekMessage while loop.

	return true;
}

void Tasks::_GetNewTaskList()
{
	DMAutoLock lockIt(&m_TaskLocker);
	if (!m_TaskRunList.empty())
	{// 非空时，清除以前的任务列表
		m_TaskRunList.clear();
	}

	m_TaskRunList.swap(m_TaskWaitList);
}

void Tasks::_DealWithTaskList()
{
	// Run task
	t_lstTaskQueue::iterator iter = this->m_TaskRunList.begin();
	t_lstTaskQueue::iterator iterEnd = this->m_TaskRunList.end();
	for ( ; iter != iterEnd; ++iter)
	{
		t_lstTaskQueue::value_type pTask = (*iter);
		if (pTask)
		{
			pTask->Run();
		}
	} // for

	// Delete task
	if (!m_TaskRunList.empty())
	{
		t_lstTaskQueue clearTaskRunList;
		clearTaskRunList.swap(m_TaskRunList);
		//
		iter = clearTaskRunList.begin();
		iterEnd = clearTaskRunList.end();
		for ( ; iter != iterEnd; ++iter) 
		{
			t_lstTaskQueue::value_type& pTask = (*iter);
			if (pTask)
			{
				pTask->Release();
			}
		} // for
		if (m_hTaskFinishEvent)
		{
			::SetEvent(m_hTaskFinishEvent);
		}
	}
}

bool Tasks::_DestroyTasks()
{
	m_bExitTaskThread = true;
	DMAutoLock lockIt(m_pThreadLocker);
	if (m_hTaskFinishEvent)
	{
		::SetEvent(m_hTaskFinishEvent);
	}
	// 关闭任务执行信号m_hTaskWaitEvent。
	if (m_hTaskWaitEvent)
	{
		::SetEvent(m_hTaskWaitEvent);
	}
	::Sleep(100);
	DM_CLOSEHANDLE(m_hTaskFinishEvent);
	DM_CLOSEHANDLE(m_hTaskWaitEvent);
	return true;
}

/// TasksThread -----------------------------------------------------------
TasksThread::TasksThread()
: m_hThread(NULL)
{

}

bool TasksThread::InitTasks(DWORD dwTimeOut /*= INFINITE*/)
{
	if (dwTimeOut > 0)
	{
		m_dwTimeOut = dwTimeOut;
	}
	return TRUE;
}

bool TasksThread::RunTasks()
{
	unsigned int uTid = 0;
	if (this->_InitTasksThread()) 
	{
		m_hThread = (HANDLE)_beginthreadex(NULL, 0, &TasksThread::ThreadProc, this, 0, &uTid);
	}

	return uTid > 0 ? true : false;
}

bool TasksThread::StopTasks()
{
	bool bNeedTerminateThread = false;
	// 要处理闪退(任务线程还没来及启动，就请求退出)
	if (false ==_IsTaskRunning()) 
	{
		::Sleep(50);
		if (NULL != m_hThread) 
		{
			::Sleep(100);
			bNeedTerminateThread = true;
		}
	}
	bool bSuccess =_UnInitTasksThread();
	if (bSuccess && NULL != m_hThread) 
	{
		DWORD dwIndex = -1;
		bSuccess =(S_OK == ::CoWaitForMultipleHandles(0,5000,1,&m_hThread,&dwIndex));
		DM_CLOSEHANDLE(m_hThread);
	}

#if 1
	if (false == bSuccess || bNeedTerminateThread)
	{
		::ExitProcess(0);
	}
	DM_CLOSEHANDLE(m_hThread);
#endif
	return bSuccess;
}

void TasksThread::OnStart()
{
}

unsigned int WINAPI TasksThread::ThreadProc(LPVOID lp)
{
	DWORD dwResult = 0;
	TasksThread* _pThis = static_cast<TasksThread*>(lp);
	if (_pThis) 
	{
		_pThis->OnStart();
		dwResult = _pThis->_RunTasksThread();
	}
	return dwResult;
}

///TaskSvc--------------------------------------------------------------------------
TaskSvc::TaskSvc()
: m_pTaskImpl(new TasksThread)
{

}

TaskSvc::~TaskSvc()
{
	DM_DELETE(m_pTaskImpl);
}

bool TaskSvc::InitTaskSvc(DWORD dwTimeOut /*= INFINITE*/)
{
	return m_pTaskImpl->InitTasks(dwTimeOut);
}

bool TaskSvc::RunTaskSvc()
{
	return m_pTaskImpl->RunTasks();
}

bool TaskSvc::StopTaskSvc()
{
	return m_pTaskImpl->StopTasks();
}

bool TaskSvc::AddTask(ITask* pTask, bool bExec /*= true*/)
{
	return m_pTaskImpl->AddNewTask(pTask, bExec);
}

bool TaskSvc::ExecTask(bool bSync /*= false*/, DWORD dwTimeOut /*= INFINITE*/)
{
	return m_pTaskImpl->ExecTask(bSync, dwTimeOut);
}
