#-------------------------------------------------------
# Copyright (c) ED
# All rights reserved.
# History:
# 		<Author>	<Time>		<Version>	  <Des>
#      lzlong		2019-1-10	1.0		
#-------------------------------------------------------
PROJECT(EDBase)

FILE(GLOB ROOT_HEADER 									inc/*.h)
FILE(GLOB COMMON_HEADER     							inc/Common/*.h)
FILE(GLOB COMMON_INLINE_HEADER     						inc/Common/Inline/*.h)
FILE(GLOB COMMON_TEMPLATE_HEADER     					inc/Common/Template/*.h)
FILE(GLOB JSON_HEADER 									inc/Json/*.h)
FILE(GLOB CRASH_HEADER     								inc/Crash/*.h)
FILE(GLOB EVENT_HEADER       							inc/Event/*.h)

FILE(GLOB ROOT_SOURCE 									src/*.cpp;src/*.c)
FILE(GLOB JSON_SOURCE 									src/Json/*.cpp;src/Json/*.c)
FILE(GLOB CRASH_SOURCE     								src/Crash/*.cpp;src/Crash/*.c)
FILE(GLOB COMMON_SOURCE     							src/Common/*.cpp;src/Common/*.c)
FILE(GLOB COMMON_INLINE_SOURCE     						src/Common/Inline/*.cpp;src/Common/Inline/*.c)
FILE(GLOB EVENT_SOURCE       							src/Event/*.cpp;src/Event/*.c)

SOURCE_GROUP("inc" 										FILES 	${ROOT_HEADER})
SOURCE_GROUP("inc\\Common" 								FILES 	${COMMON_HEADER})
SOURCE_GROUP("inc\\Common\\Inline" 						FILES 	${COMMON_INLINE_HEADER})
SOURCE_GROUP("inc\\Common\\Template" 					FILES 	${COMMON_TEMPLATE_HEADER})
SOURCE_GROUP("inc\\json" 								FILES 	${JSON_HEADER})
SOURCE_GROUP("inc\\Crash" 								FILES 	${CRASH_HEADER})
SOURCE_GROUP("inc\\Event" 								FILES 	${EVENT_HEADER})
SOURCE_GROUP("src"										FILES 	${ROOT_SOURCE})
SOURCE_GROUP("src\\Common" 								FILES 	${COMMON_SOURCE})
SOURCE_GROUP("src\\Common\\Inline" 						FILES 	${COMMON_INLINE_SOURCE})
SOURCE_GROUP("src\\json"								FILES 	${JSON_SOURCE})
SOURCE_GROUP("src\\Crash" 								FILES 	${CRASH_SOURCE})
SOURCE_GROUP("src\\Event" 								FILES 	${EVENT_SOURCE})


use_precompiled_header(EDBase
   "${CMAKE_CURRENT_SOURCE_DIR}/inc/EDBaseAfx.h"
   "${CMAKE_CURRENT_SOURCE_DIR}/src/EDBaseAfx.cpp"
  )
  
INCLUDE_DIRECTORIES(${CMAKE_CURRENT_SOURCE_DIR}/
					${CMAKE_CURRENT_SOURCE_DIR}/inc
					${CMAKE_CURRENT_SOURCE_DIR}/inc/Common
					${CMAKE_CURRENT_SOURCE_DIR}/inc/Common/Inline
					${CMAKE_CURRENT_SOURCE_DIR}/inc/Common/Template
					${CMAKE_CURRENT_SOURCE_DIR}/inc/json
					${CMAKE_CURRENT_SOURCE_DIR}/inc/Crash
					${CMAKE_CURRENT_SOURCE_DIR}/inc/Event
					)
  
INCLUDE_DIRECTORIES(${ED_SOURCE_DIR}/Modules/DmMain/inc
					${ED_SOURCE_DIR}/Modules/DmMain/inc/Common
					${ED_SOURCE_DIR}/Modules/DmMain/inc/Common/ActiveX
					${ED_SOURCE_DIR}/Modules/DmMain/inc/Common/Template
					${ED_SOURCE_DIR}/Modules/DmMain/inc/Core
					${ED_SOURCE_DIR}/Modules/DmMain/inc/Core/Msg
					${ED_SOURCE_DIR}/Modules/DmMain/inc/Core/Dui
					${ED_SOURCE_DIR}/Modules/DmMain/inc/Core/Event
					${ED_SOURCE_DIR}/Modules/DmMain/inc/IDmMain
					${ED_SOURCE_DIR}/Modules/DmMain/inc/Modules
					${ED_SOURCE_DIR}/Modules/DmMain/inc/Widgets)
					
INCLUDE_DIRECTORIES(${ED_SOURCE_DIR}/3rdParty/json)	

add_definitions(-DUNICODE -D_UNICODE -D_CRT_SECURE_NO_WARNINGS -DED_API_EXPORTS)  # unicode版本
set(CMAKE_DEBUG_POSTFIX "_d")		   # Debug下生成文件增加后缀,对exe无效.

set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /Zi")
set(CMAKE_SHARED_LINKER_FLAGS_RELEASE "${CMAKE_SHARED_LINKER_FLAGS_RELEASE} /DEBUG /OPT:REF /OPT:ICF")

ADD_LIBRARY(EDBase SHARED 
			${ROOT_HEADER}
			${COMMON_HEADER}
			${COMMON_INLINE_HEADER}
			${COMMON_TEMPLATE_HEADER}
			${JSON_HEADER}
			${CRASH_HEADER}
			${EVENT_HEADER}
			${ROOT_SOURCE}
			${COMMON_SOURCE}
			${COMMON_INLINE_SOURCE}
			${JSON_SOURCE}
			${CRASH_SOURCE}
			${EVENT_SOURCE}
			)
			
add_dependencies(EDBase  DmMain)
add_dependencies(EDBase  json)
SET_TARGET_PROPERTIES(EDBase PROPERTIES OUTPUT_NAME "EDBase")
SET_TARGET_PROPERTIES(EDBase PROPERTIES FOLDER Modules)