// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	GPDumpReport.h
// File mark:   
// File summary:copy from p2p
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-2-27
// ----------------------------------------------------------------
#pragma once

class ED_EXPORT DumpReport 
{
public:
	void InitDumpReport(LPCWSTR lpVersion = L"1.0.0.0");
	
public:
	static LPTOP_LEVEL_EXCEPTION_FILTER WINAPI TempSetUnhandledExceptionFilter(LPTOP_LEVEL_EXCEPTION_FILTER lpTopLevelExceptionFilter);
	static BOOL DumpToFile(LPCTSTR szPath, EXCEPTION_POINTERS *pException);
	static LONG WINAPI UnhandledExceptionFilterEx(_EXCEPTION_POINTERS *pException);
	static LONG WINAPI ExitUnhandledExceptionFilterEx(_EXCEPTION_POINTERS *pException);
	static void MyPureCallHandler();
	static void MyInvalidParameterHandler(const wchar_t* expression, const wchar_t* function, const wchar_t* file, unsigned int line, uintptr_t pReserved);

public:
	void SetInvalidHandle();
	void UnSetInvalidHandle();
	BOOL AddExceptionHandle();
	BOOL AddExitExceptionHandle();
	BOOL RemoveExceptionHandle();
	BOOL PreventSetUnhandledExceptionFilter();///< ��ֹ��Set��δ����
	
private:
	LPTOP_LEVEL_EXCEPTION_FILTER m_preFilter;
	_invalid_parameter_handler   m_preIph;
	_purecall_handler			 m_prePch;    
};