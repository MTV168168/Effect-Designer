// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	Base64Coder.h
// File mark:   
// File summary:
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-3-1
// ----------------------------------------------------------------
#pragma once

/// <summary>
///		Base64ת����
/// </summary>
class Base64Coder
{
public:
	long Encode(char* bin,long binlen,char* bout);
	long Decode(const char* bin,long binlen,char* bout);
	bool SetWordWrap(long lBpl);

private:
	bool				 __bWordWrap;
	long				 __lBytesPerLine;
};
